#include <iostream>
using namespace std;


class Animal
{
public:
    void virtual Voice()
    {
        cout << "Ok,let's see which voices we have..." << endl;
    }
    Animal() {}
};

class Dog :public Animal
{
public:
    void Voice() override
    {
        cout << "Woof-Woof!" << endl;
    }
    Dog() {}
};

class Cat :public Animal
{
public:
    void Voice() override
    {
        cout << "Meow!" << endl;
    }
    Cat() {}
};

class Cow :public Animal
{
public:
    void Voice() override
    {
        cout << "Moooo!" << endl;
    }
    Cow() {}
};

class Frog : public Animal
{
public:
    void Voice() override
    {
        cout << "Croak!" << endl;
    }
    Frog() {}
};

int main()
{
    int size = 5;
    Animal* p1 = new Animal;
    Dog* p2 = new Dog;
    Cat* p3 = new Cat;
    Cow* p4 = new Cow;
    Frog* p5 = new Frog;

    Animal** array = new Animal * [size] { p1, p2, p3, p4, p5};

    for (int i = 0; i < size; i++)
    {
        array[i]->Voice();

    }
}